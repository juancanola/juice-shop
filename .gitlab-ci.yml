stages:
  - runSAST
  - runSCA
  - runDAST

include:
  - template: Security/SAST.gitlab-ci.yml
  - template: DAST.gitlab-ci.yml

variables:
  SONAR_USER_HOME: "${CI_PROJECT_DIR}/.sonar"  # Defines the location of the analysis task cache
  GIT_DEPTH: "0"  # Tells git to fetch all the branches of the project, required by the analysis task
  DAST_WEBSITE: https://juice-shop.herokuapp.com/
  DAST_FULL_SCAN_ENABLED: "true" # do a full scan
  DAST_BROWSER_SCAN: "true" # use the browser-based GitLab DAST crawler

sonar-sast:
  stage: runSAST
  image:
    name: sonarsource/sonar-scanner-cli:latest
    entrypoint: [""]
  cache:
    key: "${CI_JOB_NAME}"
    paths:
      - .sonar/cache
  script:
    - sonar-scanner
  only:
    - merge_requests
    - master
    - develop
  when: manual

snyc-sca:
  stage: runSCA
  image: maven:3-openjdk-11 # Or newer
  script: |
    SNYK_TOKEN='f2825c8e-daeb-4094-b559-69108b934859'
    export SNYK_TOKEN
    mvn snyk:test -fn
  when: manual

owaspzap-dast:
  stage: runDAST
  image: docker:stable
  before_script:
    - apk --no-cache add bash curl jq wget maven openjdk11
  script: |
    wget https://github.com/zaproxy/zaproxy/releases/download/v2.12.0/ZAP_2.12.0_Linux.tar.gz
    mkdir zap
    tar -xvf ZAP_2.12.0_Linux.tar.gz
    cd ZAP_2.12.0
    ./zap.sh -cmd -quickurl $DAST_WEBSITE -quickprogress -quickout ../zap_report.xml
  artifacts:
    paths:
      - zap_report.xml

sast:
  stage: runSAST
  artifacts:
    paths:
      - gl-sast-report.json



horusec-sast:
  stage: runSAST
  image: alpine:3.18.2
  before_script:
    - apk --no-cache add bash curl jq
  script: |
     curl -fsSL "https://github.com/ZupIT/horusec/releases/download/v2.7.1/horusec_linux_amd64" -o "horusec"
     mv "horusec" "/usr/local/bin/horusec"
     chmod +x "/usr/local/bin/horusec" 
     horusec version
     horusec generate
     horusec start -p . --disable-docker="true" -o="json" -O="./horusec-report.json"
     jq -c '.analysisVulnerabilities[].vulnerabilities.severity' horusec-report.json > filejson.txt
     breaker=no
     severity=\"NONE\"
     while read -r line; do if [[ $line == $severity ]] ; then breaker=yes break; fi ; done < filejson.txt
     if [[ $breaker == "yes" ]] ; then echo "Existen vulnerabilidades por solucionar en el proyecto"; exit 1;fi
     
  artifacts:
    paths:
      - horusec-report.json
  when: manual

dast:
  stage: runDAST
  image:
    name: "$SECURE_ANALYZERS_PREFIX/dast:$DAST_VERSION$DAST_IMAGE_SUFFIX"
  variables:
    GIT_STRATEGY: none
  allow_failure: true
  script: |
     export DAST_WEBSITE=${DAST_WEBSITE:-$(cat environment_url.txt)}
     if [ -z "$DAST_WEBSITE$DAST_API_SPECIFICATION" ]; then echo "Either DAST_WEBSITE or DAST_API_SPECIFICATION must be set. See https://docs.gitlab.com/ee/user application_security/dast/#configuration for more details." && exit 1; fi
     /analyze
  artifacts:
    reports:
      dast: gl-dast-report.json
    paths:
      - gl-dast-report.json
  rules:
    - if: $DAST_DISABLED == 'true' || $DAST_DISABLED == '1'
      when: never
    - if: $DAST_DISABLED_FOR_DEFAULT_BRANCH == 'true' &&
          $CI_DEFAULT_BRANCH == $CI_COMMIT_REF_NAME
      when: never
    - if: $DAST_DISABLED_FOR_DEFAULT_BRANCH == '1' &&
          $CI_DEFAULT_BRANCH == $CI_COMMIT_REF_NAME
      when: never
    - if: $CI_DEFAULT_BRANCH != $CI_COMMIT_REF_NAME &&
          $REVIEW_DISABLED == 'true'
      when: never
    - if: $CI_DEFAULT_BRANCH != $CI_COMMIT_REF_NAME &&
          $REVIEW_DISABLED == '1'
      when: never
    - if: $CI_COMMIT_BRANCH &&
          $CI_GITLAB_FIPS_MODE == "true" &&
          $GITLAB_FEATURES =~ /\bdast\b/
      variables:
        DAST_IMAGE_SUFFIX: "-fips"
    - if: $CI_COMMIT_BRANCH &&
          $GITLAB_FEATURES =~ /\bdast\b/
  after_script:
    # Remove any debug.log files because they might contain secrets.
    - rm -f /zap/wrk/**/debug.log
    - cp -r /zap/wrk dast_artifacts